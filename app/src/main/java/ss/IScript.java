/*
Copyright (C) 2011, Doti, deviatenow.com
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
package ss;

import java.util.List;
import java.util.Map;

/**
 * <p>Functions are, with operators and various keywords, one of the things composing the language of SexScripts. It is the specific thing, the rest belonging to the base language, Groovy.</p>
 * <p>This is the API 9. If someone start a script using API 9 with an old release of SexScripts, a message will appear, asking to download a newer release.</p>
 * <p>You want more functions ? Something is missing ? Feel free to post in this forum.</p>
 */
public interface IScript {

	/**
	 * Exits software (on the contrary, simpley type <em>returns null</em> to exit script). 
	 */
	public void exit();

	/**
	 * Asks user to click yes or no, return it (true or false).
	 * <br />Example : <em>def b1 = getBoolean(&quot;Ok ?&quot;);</em>
	 * @param text will erase text (<em>null</em> to avoid this)
	 * @return true if user clicked on the first button
	 */
	public boolean getBoolean(Object text) throws InterruptedException;

	/**
	 * Asks user to click yes or no, return it (true or false).
	 * <br />Example : <em>def b2 = getBoolean(&quot;Sure ?&quot;, &quot;Yes&quot;, &quot;Quite&quot;) ;</em>
	 * @param text will erase text (<em>null</em> to avoid this)
	 * @param yesMessage set to avoid the default one (&quot;Yes&quot; in english)
	 * @param noMessage set to avoid the default one (&quot;No&quot; in english)
	 * @return true if user clicked on the first button
	 */	
	public boolean getBoolean(Object text, Object yesMessage, Object noMessage) throws InterruptedException;

	/**
	 * Asks user to click some checkboxes, returns an array of boolean (true/false). Used by options scripts (toys, etc.)
	 * @param title title of dialog window
	 * @param values available values
	 * @param defaultValues selected value at the start of this dialog window
	 * @return selected values
	 */
	public List<Boolean> getBooleans(Object title, List<String> values,
			List<Boolean> defaultValues) throws InterruptedException;

	/**
	 * Directory of images, videos, sounds
	 * @return full directory path, including the trailing slash
	 */
	public String getDataFolder();

	/**
	 * Take a webcam picture if possible, if not call getFile()
	 * @param message shown message
	 * @return full name, its path included (or <em>null</em> if he cancelled)
	 */
	public String getImage(Object message) throws InterruptedException;
	
	/**
	 * Asks user to choose a file.
	 * @param message shown message
	 * @return full name, its path included (or <em>null</em> if he cancelled)
	 */
	public String getFile(Object message) throws InterruptedException;

	/**
	 * Asks user to write a number (perhaps with decimals). 
	 * @param text will erase text (<em>null</em> to avoid this).
	 * @param defaultValue numeric default value
	 * @return numeric value, or null if failed
	 */
	public Float getFloat(Object text, Float defaultValue) throws InterruptedException;

	/**
	 * Asks user to write a number (without decimals). 
	 * @param text will erase text (<em>null</em> to avoid this).
	 * @param defaultValue numeric default value
	 * @return numeric value, or null if failed
	 */
	public Integer getInteger(Object text, Integer defaultValue) throws InterruptedException;

	/**
	 * Run random number generator
	 * @param max (excluded) maximum value, integer
	 * @return a random number between 0 and max (excluded).<br />Example : <em>if(getRandom(2) == 0) ...</em>
	 */
	public int getRandom(Integer max);

	/**
	 * Asks user to choose a value in a combobox (drop down list) 
	 * @param text will erase text (<em>null</em> to avoid this).
	 * @param values an array of values (example : ["whip", "paddle"]) 
	 * @return the number of this value (first one is 0, then 1...).
	 */	
	public int getSelectedValue(Object text, List<String> values) throws InterruptedException;

	/**
	 * Asks user to write a character string. 
	 * <br />Example : <em>def c = getString(&quot;What is your favorite color for panties ?&quot;, &quot;&quot;)</em>
	 * @param text will erase text (<em>null</em> to avoid this).
	 * @param defaultValue default value (text)
	 * @return numeric value, or null if failed
	 */
	public String getString(Object text, Object defaultValue) throws InterruptedException;
	
	/**
	 * Look for system time.
	 * @return an (big) integer, number of seconds til 01/01/1970
	 */
	public int getTime();
	
	/**
	 * @return true if there's a usable web connection
	 */
	public boolean isConnected() throws InterruptedException;
	
	/**
	 * Reads in data file (<em>data.properties</em>) a boolean (true/false) associated with a key, return it. Use this for example to know if user is a woman : <em>def isWomen = loadBoolean(&quot;intro.women&quot;);</em>. More info : read (notepad, ...) <em>data.properties</em>.
	 * @param key the key of the value, a character string, usually containing lowercase letters and dots.
	 * @return the value corresponding to the key, returns <em>null</em> if not found. 
	 */
	public Boolean loadBoolean(String key);

	/**
	 * Reads in data file some booleans, returning the first one that is true. Example : <em>loadFirstTrue(&quot;a&quot;,&quot;b&quot;)</em> will return &quot;a&quot; if there is &quot;a=true&quot; in data file, else &quot;b&quot; if there is &quot;b=true&quot;, else <em>null</em>. Useful for toys and clothes options.
	 * @param keys keys of values, character strings, separated with commas
	 * @return first key with "true" as a value
	 */
	public String loadFirstTrue(String... keys);

	/**
	 * Reads in data file a floating number associated with a key, returns it. 
	 * @param key the key of the value, a character string, usually containing lowercase letters and dots.
	 * @return value corresponding to the key, returns <em>null</em> if not found.
	 */
	public Float loadFloat(String key);

	/**
	 * Reads in data file an integer number associated with a key, returns it. 
	 * @param key the key of the value, a character string, usually containing lowercase letters and dots.
	 * @return value corresponding to the key, returns <em>null</em> if not found.
	 */
	public Integer loadInteger(String key);

	/**
	 * Reads in data file a full map of data associated with a key, returns it. 
	 * @param key the key of the value, a character string, usually containing lowercase letters and dots.
	 * @return value corresponding to the key, returns <em>null</em> if not found.
	 * @since API 8
	 */
	public Map<String,?> loadMap(String key);
	
	/**
	 * Reads in data file a string associated with a key, returns it.
	 * <br />Example : <em>show(&quot;Hello &quot;+loadString(&quot;intro.name&quot;))</em> 
	 * @param key the key of the value, a character string, usually containing lowercase letters and dots.
	 * @return value corresponding to the key, returns <em>null</em> if not found.
	 */
	public String loadString(String key);
	
	/**
	 * Reads any data in data file, returns it. 
	 * @param key the key of the value, a character string, usually containing lowercase letters and dots.
	 * @return value corresponding to the key, returns <em>null</em> if not found.
	 * @since API 8
	 */
	public Object load(String key);

	/**
	 * Try to open all CD trays.
	 */
	public void openCdTrays();

	/**
	 * Plays sound (.wav or .mp3 file must be in &quot;sounds/&quot; directory, or in a sub-directory). Does not wait for the end of the sound.
	 * @param fileName name of file, with relative path. example : "myscript/noise.mp3" 
	 */
	public void playBackgroundSound(String fileName);
	
	/**
	 * Plays sound (.wav or .mp3 file must be in &quot;sounds/&quot; directory, or in a sub-directory). Does not wait for the end of the sound.
	 * @param fileName name of file, with relative path. example : "myscript/noise.mp3"
	 * @param loop replay at the end 
	 */
	public void playBackgroundSound(String fileName, int times);

	/**
	 * Plays sound (.wav or .mp3 file must be in &quot;sounds/&quot; directory, or in a sub-directory). Wait for the end of the sound, then continue.
	 * @param fileName name of file, with relative path. example : "myscript/noise.mp3" 
	 */
	public void playSound(String fileName) throws InterruptedException;

	/**
	 * Reads in the online data file (<em>data.properties</em>) a boolean (true/false) associated with a 
	 * key, return it. Use this for example to know if user is a woman : 
	 * <em>def isWomen = receiveBoolean(&quot;intro.women&quot;);</em>. More info : read (notepad, ...) <em>data.properties</em>.
	 * @param key the key of the value, a character string, usually containing lowercase letters and dots.
	 * @return the value corresponding to the key, returns <em>null</em> if not found. 
	 */
	public Boolean receiveBoolean(String key) throws InterruptedException;

	/**
	 * Reads in the online data file a floating number associated with a key, returns it. 
	 * @param key the key of the value, a character string, usually containing lowercase letters and dots.
	 * @return value corresponding to the key, returns <em>null</em> if not found.
	 */
	public Float receiveFloat(String key) throws InterruptedException;

	/**
	 * Receive an image from online repository
	 * @param code code created by sendImage() (probably on the computer of someone else)
	 * @return new local filename
	 */
	public String receiveImage(String code) throws InterruptedException;
		
	/**
	 * Reads in the online data file an integer number associated with a key, returns it. 
	 * @param key the key of the value, a character string, usually containing lowercase letters and dots.
	 * @return value corresponding to the key, returns <em>null</em> if not found.
	 */
	public Integer receiveInteger(String key) throws InterruptedException;
	
	/**
	 * Reads in the online data file a fll map of data associated with a key, returns it. 
	 * @param key the key of the value, a character string, usually containing lowercase letters and dots.
	 * @return value corresponding to the key, returns <em>null</em> if not found.
	 */
	public Map<String,?> receiveMap(String key) throws InterruptedException;
	
	/**
	 * Reads in the online data file any data associated with a key, returns it. 
	 * @param key the key of the value, a character string, usually containing lowercase letters and dots.
	 * @return value corresponding to the key, returns <em>null</em> if not found.
	 */
	public Object receive(String key) throws InterruptedException;
	
	/**
	 * Reads in the online data file a string associated with a key, returns it.
	 * <br />Example : <em>show(&quot;Hello &quot;+loadString(&quot;intro.name&quot;))</em> 
	 * @param key the key of the value, a character string, usually containing lowercase letters and dots.
	 * @return value corresponding to the key, returns <em>null</em> if not found.
	 */
	public String receiveString(String key) throws InterruptedException;
	
	/**
	 * Saves something (String, Integer,Float, Boolean) in datafile. See loadBoolean(), loadInteger(), etc.
	 * <br />Example : <em>save("intro.name","nothing")</em>
	 * @param key string with lowercase letter and dots, of your choice
	 * @param value value of any type
	 */
	public void save(String key, Object value);
	
	/**
	 * Saves something (String, Integer,Float, Boolean) in online datafile. See receiveBoolean(), receiveInteger(), etc.
	 * <br />Example : <em>send("myscript.val3",2)</em>
	 * @param key string with lowercase letter and dots, of your choice
	 * @param value value of any type
	 **/
	public void send(String key, Object value) throws InterruptedException;
	
	/**
	 * Send an image online (in a secret location)
	 * @param filename current image on disk
	 * @return new code created by the server, or null 
	 */
	public String sendImage(String filename) throws InterruptedException;

	/**
	 * Shows image (file must be in &quot;images/&quot; directory, perhaps in a sub-directory).<br />Extensions jpg, jpeg, png, gif are supported (even with transparency). <br />Example : <em>setImage(null)</em>
	 * @param fileName file name. If filename is <em>null</em>, it hide the current image and show nothing.
	 */
	public void setImage(String fileName) throws InterruptedException;

	/**
	 * Shows image. In order to build image programatically.
	 * @param bytes
	 * @param useless boolean, not used
	 */
	public void setImage(byte[] bytes, int useless);

	/**
	 * Indicates some informations about this script. Put this one time at the start of the script. See template file (when creating a new script) for more informations.
	 */
	public void setInfos(int version, String title, String summary,
			String author, String status, int color, String lang,
			List<String> tags) throws InterruptedException ;

	/**
	 * Shows a simple message<br />Example : <em>show(&quot;You've been badt !&quot;)</em>
	 * @param message message to show
	 */
	public void show(Object message);

	/**
	 * Shows a button with some text inside, after main text. Blocks script.
	 * <br />Example : <em>showButton(&quot;Drank it&quot;)</em> 
	 * @param message Message in button
	 * @return Number of seconds waiting user's click.
	 */
	public double showButton(Object message) throws InterruptedException;
	
	/**
	 * Shows a button with some text inside, after main text. Blocks script.
	 * <br />Example : <em>showButton(&quot;Please quick click !&quot;, 3)</em>
	 * @param message Message in button
	 * @param duration Stops waiting after this duration and then hide the button and continue.
	 * @return Number of seconds waiting user's click.
	 */
	public double showButton(Object message, double duration) throws InterruptedException;

	/**
	 * Shows a popup window with some text inside. 
	 * @param message Text inside the window
	 * @return Number of seconds waiting user's click.
	 */
	public float showPopup(Object message);

	/**
	 * Asks the system to start the tool associated with email sending (<em>Outlook</em>, <em>mail</em>, ...).
	 * @param address Receiver address as a parameter.
	 */
	public void useEmailAddress(String address);
	
	/**
	 * Asks the system to open the document (video, sound...), based on his name and extension (using &quot;start&quot; on Windows, &quot;open&quot; on Mac, &quot;xdg-opn&quot; on Linux). 
	 * @param fileName file name, should be relative to the root directory of SexScripts, for example &quot;video/myscript/sexy.avi&quot;.
	 */
	public void useFile(String fileName);

	/**
	 * Asks the system to open an URL in the default web browser.
	 * @param url URL (internet address, &quot;http://....&quot;)
	 */
	public void useUrl(String url);

	/**
	 * Blocks script during some time.<br />Example : <em>wait(0.5)</em>
	 * @param duration Delay in seconds.
	 */
	public void replacedByWait(double duration) throws InterruptedException;

	/**
	 * Blocks script during some time, showing a gauge.
	 * @param duration Delay in seconds.
	 */
	public void waitWithGauge(double duration) throws InterruptedException;
	
	
	public void stopSoundThreads();
	
	public String run() throws InterruptedException;
}