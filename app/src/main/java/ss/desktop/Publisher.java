/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.desktop;

import java.awt.Cursor;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import ss.MainWindow;
import ss.ScriptContainer;
import ss.WebCommunicator;

/**
 * @author Doti
 * 
 */
public class Publisher {

	private static final String SAVE_RESOURCE_LIST_CODE = "Save this list !";
	private static final String SPECIAL_CODE_TO_AVOID_TIME_UPDATE = "maintenance release";

	private EditorFrame editorFrame;
	private MainFrame main;

	public Publisher(EditorFrame editorFrame, MainFrame main) {
		this.editorFrame = editorFrame;
		this.main = main;
	}

	public void execute() {
		ScriptContainer sc = main.getScriptContainer();
		String text = sc.currentScriptText;
		List<String> fileNames = getWantedFiles(text);
		if (fileNames == null || fileNames.size() == 0)
			return;
		if (fileNames.get(fileNames.size() - 1).equals(SAVE_RESOURCE_LIST_CODE)) {
			fileNames.remove(fileNames.size() - 1);
			text += "\n/*\n * Resources";
			try {
				String currentFolder = new File("").getCanonicalPath();
				for (String fileName : fileNames) {
					fileName = fileName.replace(currentFolder, "").replace(
							"\\", "/");
					if (fileName.startsWith("/"))
						fileName = fileName.substring(1);
					//see http://ss.deviatenow.com/viewtopic.php?p=4153#p4153
					if(fileName.startsWith("./")) 
						fileName = fileName.substring(2);
					text += "\n * " + fileName;
				}
			} catch (IOException ex) {
				JOptionPane.showMessageDialog(editorFrame,
						main.getString("error", ex.toString()));
				ex.printStackTrace();
			}
			text += "\n */";
			sc.currentScriptText = text;
			editorFrame.setText(text);
			main.writeScript(sc.currentScriptFileName, true);
		}
		editorFrame.addInfo("...");
		editorFrame.setCursor(new Cursor(Cursor.WAIT_CURSOR));
		try {
			doPublish(fileNames,
					text.contains(SPECIAL_CODE_TO_AVOID_TIME_UPDATE));
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(editorFrame,
					main.getString("error", ex.toString()));
			ex.printStackTrace();
		}
		editorFrame.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}

	private List<String> getWantedFiles(String text) {
		boolean inResources = false, hasResources = false, hasCleanedResources = false;
		ScriptContainer sc = main.getScriptContainer();
		for (String s : text.split("\n")) {
			if (s.matches("^\\s*\\*\\s*$"))
				inResources = false;
			if (inResources && s.matches("^\\s*\\*\\s*.+$")) {
				Pattern pattern = Pattern
						.compile("^\\s*\\*\\s*([^\\s].+[^\\s])\\s*$");
				Matcher matcher = pattern.matcher(s);
				if (matcher.find()) {
					sc.addToMainUsedFiles(matcher.group(1));
				}
			}
			if (s.matches("^\\s*\\*\\s*Resources\\s*$")) {
				inResources = true;
				hasResources = true;
				if (!hasCleanedResources) {
					sc.clearUsedFiles();
					hasCleanedResources = true;
				}
			}
		}

		if (sc.savedAuthor == null || sc.currentScriptFileName == null) {
			JOptionPane.showMessageDialog(editorFrame,
					main.getString("publish.please_run"));
			return null;
		}
		if (sc.savedTitle.equals("")) {
			JOptionPane.showMessageDialog(editorFrame,
					main.getString("error", "(title)"));
			return null;
		}

		JOptionPane op = new JOptionPane(main.getString("publish.files_intro",
				sc.savedTitle), JOptionPane.QUESTION_MESSAGE,
				JOptionPane.OK_CANCEL_OPTION);
		JDialog dialog = op.createDialog(main.getString("title"));

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));

		JTextArea ta = new JTextArea();
		ta.setColumns(110);
		ta.setRows(12);
		ta.setAutoscrolls(true);
		for (String s : sc.getUsedFiles())
			ta.setText(ta.getText() + s + "\n");
		JScrollPane scrollPane = new JScrollPane(ta);
		panel.add(scrollPane);
		scrollPane.setAlignmentX(0.5f);

		JCheckBox addResourcesCheckbox = new JCheckBox(
				main.getString("publish.save_resource_list"));
		if (hasResources)
			addResourcesCheckbox.setEnabled(false);
		panel.add(addResourcesCheckbox);
		addResourcesCheckbox.setAlignmentX(0.5f);

		panel.add(op);
		op.setAlignmentX(0.5f);

		dialog.setContentPane(panel);

		dialog.pack();
		dialog.setVisible(true);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		if (((Integer) op.getValue()) == JOptionPane.OK_OPTION) {
			List<String> res = new ArrayList<String>(Arrays.asList(ta.getText()
					.split("\n")));
			if (addResourcesCheckbox.isSelected())
				res.add(SAVE_RESOURCE_LIST_CODE);
			return res;
		} else
			return null;
	}

	private void addToZipStream(String fileName,
			List<String> precedingZipEntries, ZipOutputStream zos)
			throws IOException {

		String baseDir = new File("").getCanonicalPath();

		String zipName = fileName.substring(baseDir.length() + 1);
		zipName = zipName.replace("\\", "/").trim();
		if (precedingZipEntries.contains(zipName.trim()))
			return;

		String dirsPath = "";
		String[] dirs = zipName.split("/");
		dirs = Arrays.copyOfRange(dirs, 0, dirs.length - 1);
		for (String dir : dirs) {
			dirsPath += dir + "/";
			if (!precedingZipEntries.contains(dirsPath.trim())) {
				zos.putNextEntry(new ZipEntry(dirsPath));
				precedingZipEntries.add(dirsPath);
			}
		}

		zos.putNextEntry(new ZipEntry(zipName));
		int len;
		byte[] buf = new byte[1024 * 16];
		FileInputStream fis = new FileInputStream(fileName);
		while ((len = fis.read(buf)) > 0) {
			zos.write(buf, 0, len);
		}
		fis.close();
		zos.closeEntry();
	}

	private void addPathToZipStream(String pathName,
			List<String> precedingZipEntries, ZipOutputStream zos)
			throws IOException {
		String newName = pathName;
		String baseDir = new File("").getCanonicalPath();
		if (!pathName.contains(baseDir))
			newName = baseDir + "/" + MainWindow.SCRIPT_FOLDER
					+ new File(pathName).getName();
		File dir = new File(newName).getParentFile();
		if (dir.getPath().contains("*"))
			throw new IOException("\"*\" not allowed in directory names");
		if (newName.contains("*")) {
			File[] fileList = dir.listFiles(new FileFilter() {
				private String newName;

				@Override
				public boolean accept(File pathName) {
					String regexp = newName.replace("\\", "\\\\")
							.replace(".", "\\.").replace("*", ".*");
					return !pathName.isDirectory() && !pathName.isHidden()
							&& pathName.getPath().matches(regexp);
				}

				public FileFilter init(String newName) {
					this.newName = newName;
					return this;
				}
			}.init(newName));
			if (fileList != null)
				for (File f : fileList) {
					addToZipStream(f.getCanonicalPath(), precedingZipEntries,
							zos);
				}
		} else {
			addToZipStream(newName, precedingZipEntries, zos);
		}
	}

	private void doPublish(List<String> fileNames, boolean isMaintenanceRelease)
			throws IOException {
		ByteArrayOutputStream byos = new ByteArrayOutputStream();
		ZipOutputStream zos = new ZipOutputStream(byos);
		zos.setLevel(Deflater.DEFAULT_COMPRESSION);

		Collections.sort(fileNames);

		List<String> precedingZipEntries = new ArrayList<String>();
		for (String s : fileNames) {
			if (s.length() > 5)
				addPathToZipStream(s, precedingZipEntries, zos);
		}
		zos.flush();
		zos.close();
		byos.close();

		String timeStr = String.valueOf(new Date().getTime() / 1000);

		ScriptContainer sc = main.getScriptContainer();
		File localDir = new File(".", "published");
		localDir.mkdirs();
		// zipname : see scripts.php
		String zipName = new File(sc.currentScriptFileName).getName();
		zipName = zipName.substring(0, zipName.lastIndexOf("."));
		zipName += "_" + timeStr + ".zip";
		File localZipFile = new File(localDir, zipName);
		FileOutputStream fos = new FileOutputStream(localZipFile);
		fos.write(byos.toByteArray(), 0, byos.size());
		fos.close();
		System.out.println(localZipFile.getCanonicalPath());

		String encodedString = "";
		encodedString += "\n--BoBoBoBo\n";
		encodedString += "Content-Disposition: form-data; name=\"version\"\n";
		encodedString += "Content-Type: text/plain; charset=UTF-8\n\n";
		encodedString += String.valueOf(sc.savedVersion);
		encodedString += "\n--BoBoBoBo\n";
		encodedString += "Content-Disposition: form-data; name=\"title\"\n";
		encodedString += "Content-Type: text/plain; charset=UTF-8\n\n";
		encodedString += sc.savedTitle;
		encodedString += "\n--BoBoBoBo\n";
		encodedString += "Content-Disposition: form-data; name=\"summary\"\n";
		encodedString += "Content-Type: text/plain; charset=UTF-8\n\n";
		encodedString += sc.savedSummary;
		encodedString += "\n--BoBoBoBo\n";
		encodedString += "Content-Disposition: form-data; name=\"author\"\n";
		encodedString += "Content-Type: text/plain; charset=UTF-8\n\n";
		encodedString += sc.savedAuthor;
		encodedString += "\n--BoBoBoBo\n";
		encodedString += "Content-Disposition: form-data; name=\"status\"\n";
		encodedString += "Content-Type: text/plain; charset=UTF-8\n\n";
		encodedString += sc.savedStatus;
		encodedString += "\n--BoBoBoBo\n";
		encodedString += "Content-Disposition: form-data; name=\"color\"\n";
		encodedString += "Content-Type: text/plain; charset=UTF-8\n\n";
		encodedString += String.valueOf(sc.savedColor);
		encodedString += "\n--BoBoBoBo\n";
		encodedString += "Content-Disposition: form-data; name=\"lang\"\n";
		encodedString += "Content-Type: text/plain; charset=UTF-8\n\n";
		encodedString += sc.savedLang;
		encodedString += "\n--BoBoBoBo\n";
		String tags = "";
		String sep = "";
		for (String tag : sc.savedTags) {
			tags += sep + tag;
			sep = ",";
		}
		encodedString += "Content-Disposition: form-data; name=\"tags\"\n";
		encodedString += "Content-Type: text/plain; charset=UTF-8\n\n";
		encodedString += tags;
		encodedString += "\n--BoBoBoBo\n";
		encodedString += "Content-Disposition: form-data; name=\"time\"\n";
		encodedString += "Content-Type: text/plain; charset=UTF-8\n\n";
		encodedString += timeStr;
		encodedString += "\n--BoBoBoBo\n";
		if (isMaintenanceRelease) {
			encodedString += "Content-Disposition: form-data; name=\"maintenance\"\n";
			encodedString += "Content-Type: text/plain; charset=UTF-8\n\n";
			encodedString += "on";
			encodedString += "\n--BoBoBoBo\n";
		}
		encodedString += "Content-Disposition: form-data; name=\"file\"; filename=\""
				+ new File(sc.currentScriptFileName).getName() + "\"\n";
		encodedString += "Content-Transfert-encoding: binary\n";
		encodedString += "Content-Type: binary/zip\n\n";

		HttpURLConnection conn = (HttpURLConnection) new WebCommunicator()
				.getConnection(MainWindow.WEBSITE_SECURE_URL + "/scripts.php");

		try {
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setReadTimeout(120 * 1000);
			conn.setRequestProperty("Accept",
					"text/html,application/xhtml+xml,"
							+ "application/xml;q=0.9,*/*;q=0.8");
			conn.setRequestProperty("Content-Type",
					"multipart/form-data; boundary=BoBoBoBo");
			conn.setRequestProperty("Accept-Encoding", "deflate");
			OutputStream os = conn.getOutputStream();
			PrintWriter pw = new PrintWriter(new OutputStreamWriter(os, "utf-8"), true);
			pw.write(encodedString);
			pw.flush();
			os.write(byos.toByteArray(), 0, byos.size()); 
			os.flush();
			pw.write("\n--BoBoBoBo--\n");
			pw.flush();
			/*
			 * bug 323 ? String allResponse = ""; BufferedReader rd = new
			 * BufferedReader(new InputStreamReader( conn.getInputStream()));
			 * String line; while ((line = rd.readLine()) != null) { allResponse
			 * += line; } rd.close();
			 */
			byte[] buffer = new byte[1024];
			InputStream is = conn.getInputStream();
			is.read(buffer, 0, 1024);
			String allResponse = "";
			try {
				allResponse = new String(buffer);
			} catch (Exception e) {

			} finally {
				is.close();
			}
			if (!allResponse.startsWith("ok"))
				throw new Exception("Perhaps error publishing : " + allResponse);
			JOptionPane.showMessageDialog(editorFrame,
					main.getString("publish.ok"));
		} catch (Exception e) {
			e.printStackTrace();
			InputStream eis = conn.getErrorStream();
			String errorMessage = "";
			if (eis != null) {
				BufferedReader erd = new BufferedReader(new InputStreamReader(
						eis));
				String line;
				while ((line = erd.readLine()) != null) {
					errorMessage += line;
				}
			}
			JOptionPane
					.showMessageDialog(
							editorFrame,
							main.getString("error", e.toString() + "\n"
									+ errorMessage));
		}
	}

}
