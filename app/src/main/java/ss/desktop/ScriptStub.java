/*
    Copyright (C) 2012, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.desktop;

import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import ss.IScript;
import ss.MainWindow;
import ss.ScriptContainer;
import ss.TextSource;

/**
 * <p>
 * This class is extended by scripts for testing for errors
 * </p>
 * 
 * @author Doti
 */
public class ScriptStub implements IScript {

	private static final int EXTREME_DURATION_S = 30 * 24 * 3600;
	private MainWindow mainWindow;
	private TextSource textSource;
	private Random random;
	private int statDelay;
	private int state;

	public ScriptStub(MainWindow mainWindow, TextSource textSource) {
		throw new RuntimeException("Not implemented");
	}

	public ScriptStub(MainWindow mainWindow, TextSource textSource, long seed) {
		this.mainWindow = mainWindow;
		this.textSource = textSource;
		random = new Random(seed);
		statDelay = 0;
		state = 0;
	}

	public void setInfos(int version, String title, String summary,
			String author, String status, int color, String lang,
			List<String> tags) {
		state = 1;
		mainWindow.getScriptContainer().update(version, title, summary, author,
				status, color, lang, tags);
		if (version > MainWindow.API_VERSION) {
			mainWindow.showMessage(textSource.getString("error.version",
					version, MainWindow.API_VERSION), true);
		}
	}

	public void show(Object s) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
	}

	public double showButton(Object s, double duration) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		double realDuration;
		if (duration > EXTREME_DURATION_S - 0.1
				&& duration < EXTREME_DURATION_S + 0.1)
			realDuration = getRndSeconds();
		else {
			if (random.nextInt(2) == 0)
				realDuration = duration;
			else
				realDuration = duration * random.nextDouble();
		}
		statDelay += realDuration;
		return realDuration;
	}

	public double showButton(Object s) {
		return showButton(s, EXTREME_DURATION_S);
	}

	public float showPopup(Object s) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		float realDuration = getRndSeconds();
		statDelay += realDuration;
		return realDuration;
	}

	public String getDataFolder() {
		String s = mainWindow.getDataFolder().getAbsolutePath()+"/";
	    if(s.endsWith("./"))
	    	s = s.substring(0, s.length()-2);
	    return s;
	}
	
	public String getString(Object text, Object defaultValue) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		String s = "";
		for (int i = 0; i < random.nextInt(10); i++)
			s += (char) (random.nextInt(26) + 65);
		statDelay += 15;
		return s;
	}

	public Integer getInteger(Object text, Integer defaultValue) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		statDelay += getRndSeconds();
		if (random.nextInt(2) == 0)
			return defaultValue;
		else
			return random.nextInt(10) * random.nextInt(10);
	}

	public Float getFloat(Object text, Float defaultValue) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		statDelay += getRndSeconds();
		if (random.nextInt(2) == 0)
			return defaultValue;
		else
			return random.nextFloat() * random.nextInt(10) * random.nextInt(10);
	}

	public boolean getBoolean(Object text, Object yesMessage, Object noMessage) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		statDelay += 5;
		return random.nextInt(2) == 0;
	}

	public boolean getBoolean(Object text) {
		return getBoolean(text, "", "");
	}

	public int getSelectedValue(Object text, List<String> values) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		statDelay += 5 + values.size() / 2;
		return random.nextInt(values.size());
	}

	public List<Boolean> getBooleans(Object title, List<String> values,
			List<Boolean> defaultValues) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		statDelay += 1 + values.size() * 2;
		ArrayList<Boolean> res = new ArrayList<Boolean>();
		for (int i = 0; i < values.size(); i++)
			res.add(random.nextInt(2) == 0);
		return res;
	}

	public int getRandom(Integer max) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		if (max == null)
			max = 100;
		return random.nextInt(max);
	}

	public int getRandom(double max) {
		return getRandom(new Integer((int) max));
	}

	public void replacedByWait(double duration) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		statDelay += duration;
	}

	public void waitWithGauge(double duration) {
		replacedByWait(duration);
	}

	public int getTime() {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		return (int) (Math.round((System.currentTimeMillis() + TimeZone
				.getDefault().getOffset(System.currentTimeMillis())) / 1000.0))
				+ random.nextInt(10000);
	}

	public String getImage(Object message) {
		return getFile(message);
	}

	public String getFile(Object message) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		statDelay += 20;
		if (random.nextInt(2) == 0)
			return null;
		else
			return getString(null, "x.jpg");
	}

	public String loadString(String key) {
		state = 2;
		if (random.nextInt(2) == 0)
			return getString(null, "xyz");
		else
			return mainWindow.getPropertiesWorker().loadString(key);
	}

	public Integer loadInteger(String key) {
		state = 2;
		if (random.nextInt(2) == 0)
			return getInteger(null, 1);
		else
			return mainWindow.getPropertiesWorker().loadInteger(key);
	}

	public Float loadFloat(String key) {
		state = 2;
		if (random.nextInt(2) == 0)
			return getFloat(null, 1f);
		else
			return mainWindow.getPropertiesWorker().loadFloat(key);
	}

	public Boolean loadBoolean(String key) {
		state = 2;
		if (random.nextInt(2) == 0)
			return getBoolean(null);
		else
			return mainWindow.getPropertiesWorker().loadBoolean(key);
	}

	@Override
	public Map<String, ?> loadMap(String key) {
		state = 2;
		if (random.nextInt(2) == 0)
			return null;
		else
			return mainWindow.getPropertiesWorker().loadMap(key);
	}
	
	@Override
	public Object load(String key) {
		state = 2;
		if (random.nextInt(2) == 0)
			return null;
		else
			return mainWindow.getPropertiesWorker().load(key);
	}
	
	public String loadFirstTrue(String... keys) {
		state = 2;
		if (random.nextInt(2) == 0) {
			int n = random.nextInt(keys.length + 1);
			if (n == keys.length)
				return null;
			else
				return keys[n];
		} else
			return mainWindow.getPropertiesWorker().loadFirstTrue(keys);
	}

	public void save(String key, Object v) {
		state = 2;
		mainWindow.getPropertiesWorker().save(key, v);
	}

	public void setImage(String fileName) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		if (fileName == null || fileName.trim().isEmpty()) {
			// nothing
		} else {
			File f;
			if (fileName.startsWith("/") || fileName.contains(":"))
				f = new File(fileName);
			else {
				f = new File(mainWindow.getDataFolder(),
						MainWindow.VIDEO_FOLDER + fileName);
				if (!f.exists()) {
					f = new File(mainWindow.getDataFolder(),
							MainWindow.IMAGE_FOLDER + fileName);
				}
				if (f.exists()) {
					addToMainUsedFiles(f.getPath());
					// check case problems (file and its parent directory)
					// getCanonicalPath returns the real existing name, but has
					// cache...
					File dir = f.getParentFile();
					File superDir = dir.getParentFile();
					try {
						String fPath = f.getPath().replace("\\", "/");
						String fLowerCasePath = f.getPath().replace("\\", "/")
								.toLowerCase();
						for (String dirFile : dir.list())
							if (!fPath.contains("/" + dirFile)
									&& fLowerCasePath.contains("/"
											+ dirFile.toLowerCase()))
								throw new UnsupportedOperationException(
										"Image path problem ("
												+ fileName
												+ "), probable cause is case sensitivity.");
						for (String dirFile : superDir.list())
							if (!fPath.contains("/" + dirFile + "/")
									&& fLowerCasePath.contains("/"
											+ dirFile.toLowerCase() + "/"))
								throw new UnsupportedOperationException(
										"Image path problem ("
												+ fileName
												+ "), probable cause is case sensitivity.");
					} catch (UnsupportedOperationException ex) {
						throw ex;
					} catch (Exception ex) {
						// silence, it won't be easy to debug anyway
					}
				}
			}
		}
	}

	public void setImage(byte[] bytes, int unu) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
	}

	public void setImage(Image image, boolean unu) {
		ScriptContainer sc = mainWindow.getScriptContainer();
		if (sc.savedVersion > 4)
			throw new RuntimeException("Deprecated");
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
	}

	public void useFile(String fileName) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		addToMainUsedFiles(fileName);
		statDelay += 15;
	}

	public void useUrl(String url) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		statDelay += 60;
	}

	public void useEmailAddress(String address) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		statDelay += 60;
	}

	public void playSound(String fileName) {
		playBackgroundSound(fileName);
		if (fileName != null) {
			statDelay += 3;
		}
	}

	public void playBackgroundSound(String fileName) {
		playBackgroundSound(fileName, 1); 
	}
	
	public void playBackgroundSound(String fileName, int times) {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		if (fileName != null) {
			File f = new File(mainWindow.getDataFolder(),
					MainWindow.SOUND_FOLDER + fileName);
			addToMainUsedFiles(f.getPath());
		}
	}

	public void openCdTrays() {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
		statDelay += 5;
	}

	public void exit() {
		if (state == 0)
			throw new UnsupportedOperationException(
					"setInfo() must be called before everything else.");
		state = 2;
	}

	@Override
	public boolean isConnected() {
		return (random.nextInt(2) == 0);
	}

	@Override
	public Boolean receiveBoolean(String key) {
		statDelay++;
		return loadBoolean(key);
	}

	@Override
	public Float receiveFloat(String key) {
		statDelay++;
		return loadFloat(key);
	}

	@Override
	public String receiveImage(String code) {
		statDelay += 5;
		return "";
	}

	@Override
	public Integer receiveInteger(String key) {
		statDelay++;
		return loadInteger(key);
	}

	@Override
	public Map<String, ?> receiveMap(String key) throws InterruptedException {
		statDelay += 5;
		return loadMap(key);
	}
	
	@Override
	public String receiveString(String key) {
		statDelay++;
		return loadString(key);
	}
	
	@Override
	public Object receive(String key) {
		statDelay++;
		return load(key);
	}

	@Override
	public void send(String key, Object value) {
		statDelay++;
	}

	@Override
	public String sendImage(String filename) {
		statDelay += 20;
		return "abcdefgh";
	}

	public int getStatDelay() {
		return Math.max(0, statDelay);
	}

	@Override
	public void stopSoundThreads() {
	}

	@Override
	public String run() {
		return null;
	}

	// slower and slower
	private int getRndSeconds() {
		return (int) (1 + Math.pow(random.nextDouble(), 6)
				* (2 + statDelay / 2));
	}

	private void addToMainUsedFiles(String fileName) {
		mainWindow.getScriptContainer().addToMainUsedFiles(fileName);
	}
}
