/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.desktop;

import java.awt.Cursor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import groovy.json.JsonSlurper;
import ss.MainWindow;
import ss.TextSource;

/**
 * <p>
 * Called by editor.
 * </p>
 * <p>
 * Import Milovana HTML teases and Flash teases.
 * </p>
 * 
 * @author Doti
 */
public class MilovanaImportator extends Importator {

	private static final int MILOVANA_DELAY_LETTERS_1S = 18;
	private static final int MILOVANA_BASE_DELAY = 2;
	private static final int STAR_RANDOM_NB_RESOURCES = 6;
	private static final boolean NOCACHE = false;

	private EditorFrame editor;

	@Override
	public String doImport(TextSource textSource, EditorFrame editor) {
		this.editor = editor;
		try {
			String clipboardUrl = editor.getClipboardUrl();
			String url = JOptionPane.showInputDialog(textSource.getString("milovana.url"), clipboardUrl);
			if (url == null)
				return null;
			if (!url.contains("://"))
				url = "http://" + url;

			if (url.contains("showflash"))
				return doImportFlash(textSource, editor, url);
			else
				return doImportTease(textSource, editor, url);

		} catch (Exception e) {
			editor.addInfo(textSource.getString("error", e.toString()));
			editor.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			e.printStackTrace();
			return null;
		}
	}

	private String doImportTease(TextSource textSource, EditorFrame editor, String url) throws IOException {
		String res = "";

		String baseUrl = url;
		baseUrl = baseUrl.substring(0, 9 + baseUrl.substring(8).indexOf("/"));
		int withDelay = JOptionPane.showConfirmDialog(editor, textSource.getString("milovana.delay"));
		if (withDelay == JOptionPane.CANCEL_OPTION)
			return null;
		boolean writeDelays = withDelay == JOptionPane.YES_OPTION;
		res += "\n\n// Milovana - " + url + "\n// " + textSource.getString("milovana.rights");
		JOptionPane.showMessageDialog(editor, textSource.getString("milovana.please_wait"));
		editor.setCursor(new Cursor(Cursor.WAIT_CURSOR));
		int id = 0;
		String imageFolder = "milovana/";
		String mp3Folder = "milovana/";
		if (url.contains("id=")) {
			String idStr = url.substring(url.indexOf("id=") + 3);
			if (idStr.contains("&"))
				idStr = idStr.substring(0, idStr.indexOf("&"));
			id = Integer.parseInt(idStr);
			imageFolder += id + "/";
			mp3Folder += id + "/";
		}
		int n = 0;
		Set<String> images = new HashSet<String>();
		Set<String> mp3s = new HashSet<String>();
		do {
			editor.addInfo(url);
			String html = editor.getUrlContent(url);

			if (html.contains("FlashTease #"))
				return doImportFlash(textSource, editor, url);

			if (html.contains("https://eosscript.com?host"))
				return doImportEos(textSource, editor, url, id, html, writeDelays);

			String image = null, mp3 = null, text = null;
			Pattern pattern = Pattern.compile("src=\"(https?://[^\"]*)\"[^>]*class\\s?=\\s?\"tease_pic\"");
			Matcher matcher = pattern.matcher(html);
			if (matcher.find()) {
				try {
					String imageUrl = matcher.group(1);

					image = getDownloadedFile(MainWindow.IMAGE_FOLDER + imageFolder, n, images, imageUrl, editor);
				} catch (IOException e) {
				}
			}

			pattern = Pattern.compile("<a href=\"(https?://media.milovana.com/timg[^\"]*\\.mp3)\"");
			matcher = pattern.matcher(html);
			if (matcher.find()) {
				try {
					String mp3Url = matcher.group(1);
					mp3 = getDownloadedFile(MainWindow.SOUND_FOLDER + mp3Folder, n, mp3s, mp3Url, editor);
				} catch (IOException e) {
				}
			}

			pattern = Pattern.compile("<p\\s+class=\"text\"\\s*>(([^<]*<br\\s*/>)*[^<]*)</p>");
			matcher = pattern.matcher(html);
			if (matcher.find()) {
				text = matcher.group(1).trim().replace("\n", "");
				text = text.replace("<br />", "\n").replace("\"", "\\\"").replace("$", "\\$");
				text = text.replace("\n", "\\n\"+\n\t\"");
				if (text != null) {
					if (image != null)
						res += "\nsetImage(\"" + imageFolder + image + "\")";
					res += "\nshow(\"" + text + "\")" + "\n";
					if (mp3 != null)
						res += "\nplaySound(\"" + mp3Folder + mp3 + "\")";
					if (writeDelays && mp3 == null)
						res += "wait(" + (int) (MILOVANA_BASE_DELAY + text.length() / MILOVANA_DELAY_LETTERS_1S) + ")";
					if (!writeDelays)
						res += "showButton(\"Continue\")";
					res += "\n";
				}
			}

			pattern = Pattern.compile("<p\\s+class=\"link\"\\s*>\\s*<a\\s+href=\"([^\"]*)\"");
			matcher = pattern.matcher(html);
			if (matcher.find()) {
				url = matcher.group(1);
				if (!url.contains("//"))
					url = baseUrl + url;
			} else {
				url = null;
			}
			n++;
		} while (url != null && n < 1000);
		if (n == 1000)
			editor.addInfo(textSource.getString("error", "1000 URL ?"));
		res += "\nshow(\"Goodbye\")";
		editor.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		editor.addInfo("OK");
		return res;
	}

	private String doImportFlash(TextSource textSource, EditorFrame editor, String url) throws IOException {
		Pattern pattern = Pattern.compile("(\\d{2,7})");
		Matcher matcher = pattern.matcher(url);
		if (!matcher.find()) {
			throw new IllegalArgumentException("Incomplete URL");
		}
		JOptionPane.showMessageDialog(editor, textSource.getString("milovana.please_wait"));
		editor.setCursor(new Cursor(Cursor.WAIT_CURSOR));

		String id = matcher.group(1);
		String baseHtml = editor.getUrlContent(url);
		pattern = Pattern.compile("author=(\\d{1,7})");
		matcher = pattern.matcher(baseHtml);
		if (!matcher.find()) {
			throw new IllegalArgumentException("No author ?");
		}
		String authorId = matcher.group(1);
		String baseResourceString = "http://www.milovana.com/media/get.php?folder=" + authorId + "/" + id + "&name=";

		String scriptUrl = "http://www.milovana.com/webteases/getscript.php?id=" + id;
		editor.addInfo(textSource.getString("Content URL : " + scriptUrl));

		String[] lines = editor.getUrlContent(scriptUrl).split("\n");
		List<String> pageCodes = new ArrayList<String>();
		String realLine = "";
		for (String line : lines) {
			realLine += line.trim();
			if (realLine.endsWith(")")) {
				pageCodes.add(realLine);
				realLine = "";
			}
		}
		if (pageCodes.size() < 2)
			throw new IllegalArgumentException("Only " + pageCodes.size() + " pages, aborting");
		boolean probablyBig = pageCodes.size() > 20;
		String startBlock = "";
		String content = "";

		Pattern patternFullPage = Pattern.compile("(\\w+#)\\{?page\\((.*)\\)");
		Pattern patternPageText = Pattern.compile("text:'(([^']+|\\\\')*[^\\\\])'");
		Pattern patternFirstColor = Pattern.compile("COLOR=\"#([^\"]+)\"");
		Pattern patternPagePic = Pattern.compile("media:pic\\(id:(\"|')([^\"']*)(\"|')");
		// action:vert(e0:buttons(target0:p62#,cap0:"E"),e1:delay(time:3sec,target:#))
		Pattern patternSound = Pattern.compile("(hidden|action|e0|e1|e2|e3|instruc):sound\\(id:(\"|')([^\"']*)(\"|')");
		Pattern patternPageActionGo = Pattern.compile("(action|e0|e1|e2|e3|instruc):go\\(target:([^\\)]*)\\)");
		Pattern patternPageActionYn = Pattern.compile("(action|e0|e1|e2|e3|instruc):yn\\(yes:([^,]*),no:([^\\)]*)\\)");
		// action:vert(e0:buttons(target0:dead#,cap0:"quit(dead)"),e1...
		Pattern patternPageActionButtons = Pattern
				// .compile("(action|e0|e1|e2|e3|instruc):buttons\\((([^'\"]*((\"[^\"]*\")|('[^\"]*')))+[^\\)]*)\\)");
				.compile(
						"(action|e0|e1|e2|e3|instruc):buttons\\((target\\d+:\\w+#,cap\\d+:\"[^\"]*\"(,target\\d+:\\w+#,cap\\d+:\"[^\"]*\")*)\\)");
		// System.out.println(patternPageActionButtons.toString()+"\n");
		// action:delay(time:40sec,target:page15#), ignore delay w/o targer
		Pattern patternDelay = Pattern.compile("(action|e0|e1|e2|e3|instruc):delay\\("
				+ "time:(((\\d+)(\\w+))|(random\\(min:(\\d+)(\\w+),max:(\\d+)(\\w+)\\))),"
				+ "target:(([^#]+#)|(range\\(from:(\\d+),to:(\\d+)[^\\)]*\\)))(,style:([^\\)]*))?\\)");

		String imageFolder = "milovana/";
		String mp3Folder = "milovana/";
		imageFolder += id + "/";
		mp3Folder += id + "/";
		Set<String> images = new HashSet<String>();
		Set<String> mp3s = new HashSet<String>();

		String firstColor = null;

		int n = 1; // default page number
		for (String pageCode : pageCodes) {

			if (pageCode.endsWith("text:'',media:pic(id:\"*.jpg\"))"))
				continue;

			// System.out.println("### "+pageCode);

			matcher = patternFullPage.matcher(pageCode);
			if (matcher.find()) {
				String pageName = matcher.group(1);
				String pageContent = matcher.group(2);
				if (startBlock.equals(""))
					startBlock = pageName;

				if (probablyBig)
					content += "case \"" + pageName + "\":{->";
				else
					content += "case \"" + pageName + "\":";

				String image = null, mp3 = null, text = null;
				Matcher m2 = patternPageText.matcher(pageContent);
				if (m2.find())
					text = m2.group(1);
				else
					text = "";
				Matcher mFirstColor = patternFirstColor.matcher(text);
				if (mFirstColor.find())
					firstColor = mFirstColor.group(1);
				m2 = patternPagePic.matcher(pageContent);
				if (m2.find()) {
					try {
						String imageUrl = baseResourceString + m2.group(2);
						image = getDownloadedFile(MainWindow.IMAGE_FOLDER + imageFolder, n, images, imageUrl, editor);
					} catch (IOException e) {
					}
				}
				m2 = patternSound.matcher(pageContent);
				if (m2.find()) {
					try {
						String mp3Url = baseResourceString + m2.group(3);
						mp3 = getDownloadedFile(MainWindow.SOUND_FOLDER + mp3Folder, n, mp3s, mp3Url, editor);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				// groovy
				text = text.replace("$", "\\$");
				// java
				text = text.replace("\"", "\\\"");
				// strange bug, show "cant&apost" for example for can't
				text = text.replace("&apos;", "'");
				if (image != null)
					if (image.contains("*"))
						content += "\n\tsetImage(\"" + imageFolder + image + "\".replace(\"*\",\"\"+getRandom("
								+ STAR_RANDOM_NB_RESOURCES + "))" + ")";
					else
						content += "\n\tsetImage(\"" + imageFolder + image + "\")";
				// too big text
				// 12->5, 24->8
				for (int s = 5; s < 40; s++)
					text = text.replace("SIZE=\\\"" + s + "\\\"", "SIZE=\\\"" + ((int) (2 + s / 4)) + "\\\"");
				content += "\n\tshow(\"" + text + "\")";
				if (mp3 != null)
					if (mp3.contains("*"))
						content += "\n\tplayBackgroundSound(\"" + mp3Folder + mp3 + "\".replace(\"*\",\"\"+getRandom("
								+ STAR_RANDOM_NB_RESOURCES + "))" + ")";
					else
						content += "\n\tplayBackgroundSound(\"" + mp3Folder + mp3 + "\")";
				boolean goneSomewhere = false;
				boolean delayWasUsed = false;
				boolean hasDelay = false;
				boolean delayIsSecret = false;
				String delayDurationStr = "";
				String blockAfterDelayStr = null;
				m2 = patternDelay.matcher(pageContent);
				if (m2.find()) {
					int delayDuration = 0;
					int delayAddRandom = 0;
					hasDelay = true;
					if (m2.group(4) != null) {
						delayDuration = Integer.parseInt(m2.group(4));
						if (m2.group(5).equals("min"))
							delayDuration *= 60;
						if (m2.group(5).equals("hrs"))
							delayDuration *= 3600;
					} else {
						delayDuration = Integer.parseInt(m2.group(7));
						if (m2.group(8).equals("min"))
							delayDuration *= 60;
						if (m2.group(8).equals("hrs"))
							delayDuration *= 3600;
						delayAddRandom = Integer.parseInt(m2.group(9));
						if (m2.group(10).equals("min"))
							delayAddRandom *= 60;
						if (m2.group(10).equals("hrs"))
							delayAddRandom *= 3600;
						delayAddRandom -= delayDuration;
					}
					delayDurationStr = "" + delayDuration;
					if (delayAddRandom > 0)
						delayDurationStr = "(" + delayDuration + "+getRandom(" + delayAddRandom + "))";
					if (m2.group(12) != null)
						blockAfterDelayStr = "block=\"" + m2.group(12) + "\"";
					else
						blockAfterDelayStr = "block=(" + m2.group(14) + "+ getRandom("
								+ (1 + Integer.parseInt(m2.group(15)) - Integer.parseInt(m2.group(14))) + "))+\"#\"";
					delayIsSecret = m2.group(16) != null;
				}

				m2 = patternPageActionGo.matcher(pageContent);
				if (m2.find()) {
					content += "\n\tblock=\"" + m2.group(2) + "\"";
					if (hasDelay) {
						content += "\n\tif(showButton(\"Continue\"," + delayDurationStr + ")>=" + delayDurationStr + ")"
								+ "\n\t\t" + blockAfterDelayStr;
						delayWasUsed = true;
					} else {
						content += "\n\tshowButton(\"Continue\")";
					}
					goneSomewhere = true;
				}
				m2 = patternPageActionYn.matcher(pageContent);
				if (m2.find()) {
					if (hasDelay)
						content += "\n\tdef startTime" + n + " = getTime()";
					content += "\n\tif(getBoolean(null))" + "\n\t\tblock=\"" + m2.group(2) + "\""
							+ "\n\telse\n\t\tblock=\"" + m2.group(3) + "\"";
					if (hasDelay) {
						content += "\n\tdef endTime" + n + " = getTime()" + "\n\tif(endTime" + n + " - startTime" + n
								+ " > " + delayDurationStr + ")" + "\n\t\t" + blockAfterDelayStr;
						delayWasUsed = true;
					}
					goneSomewhere = true;
				}
				m2 = patternPageActionButtons.matcher(pageContent);
				if (m2.find()) {
					// System.out.println(m2.group(2));
					String[] buttons = m2.group(2).split(",targ");
					Pattern patternOneButton = Pattern.compile("(targ)?et(\\d)+:([^,]+),cap(\\d+):"
							+ "(('(([^']|\\\\')+)')|" + "(\"(([^\"]|\\\\\")+)\"))");
					if (buttons.length == 1) {
						Matcher m3 = patternOneButton.matcher(buttons[0]);
						m3.find();

						// if(!m3.matches() || m3.groupCount()<11)
						// System.out.println(" * "+buttons[0]);//+" : doesn't
						// match ?");

						content += "\n\tblock=\"" + m3.group(3) + "\"";
						String btText = m3.group(7);
						if (btText == null)
							btText = m3.group(10);
						if (hasDelay) {
							content += "\n\tif(showButton(\"" + btText + "\"," + delayDurationStr + ")>="
									+ delayDurationStr + ")" + "\n\t\t" + blockAfterDelayStr;
							delayWasUsed = true;
						} else {
							content += "\n\tshowButton(\"" + btText + "\")";
						}
					} else {
						if (hasDelay)
							content += "\n\tdef startTime" + n + " = getTime()";
						content += "\n\tdef pages" + n + "=[]";
						content += "\n\tdef values" + n + "=[]";
						for (String button : buttons) {
							Matcher m3 = patternOneButton.matcher(button);
							if (m3.find()) {
								String btText = m3.group(7);
								if (btText == null)
									btText = m3.group(10);
								content += "\n\tpages" + n + ".add(\"" + m3.group(3) + "\")";
								content += "\n\tvalues" + n + ".add(\"" + btText + "\")";
							}
						}
						content += "\n\tblock = pages" + n + "[getSelectedValue(null, values" + n + ")]";
						if (hasDelay) {
							content += "\n\tdef endTime" + n + " = getTime()" + "\n\tif(endTime" + n + " - startTime"
									+ n + " > " + delayDurationStr + ")" + "\n\t\t" + blockAfterDelayStr;
							delayWasUsed = true;
						}
					}
					goneSomewhere = true;
				}

				if (hasDelay && !delayWasUsed) {
					if (delayIsSecret)
						content += "\n\twait(" + delayDurationStr + ")";
					else
						content += "\n\twaitWithGauge(" + delayDurationStr + ")";
					content += "\n\t" + blockAfterDelayStr;
					goneSomewhere = true;
				}
				if (!goneSomewhere)
					content += "\n\tendReached = true";
				if (mp3 != null && !mp3.equals(""))
					content += "\n\tplayBackgroundSound(null);";
				if (probablyBig)
					content += "\n\t}(); break\n\n";
				else
					content += "\n\tbreak\n\n";
			}
			n++;
		}
		editor.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		// watch blue component of first text color to say dark or light
		if (firstColor != null && Integer.parseInt(firstColor, 16) % 256 > 127)
			editor.addInfo("OK, you should set a dark background in setInfo() ");
		else
			editor.addInfo("OK");

		return "\n\n// Milovana - " + url + "\n// " + textSource.getString("milovana.rights") + "\n"
				+ "def endReached = false\n" + "def block = \"" + startBlock + "\"\n" + "def newBlock = block\n"
				+ "while(!endReached && newBlock!=null) {\n" + "block = null\n" + "switch(newBlock) {\n" + content
				+ "default:\n" + "\tshow(\"Unfinished ? (\"+newBlock+\")\")\n"
				+ "\tendReached = true\n} // end switch \n" + "newBlock = block\n" + "} // end while \n"
				+ "wait(1)\nshowButton(\"Close\")\nshow(\"\")\n";
	}

	@SuppressWarnings("unchecked")
	private String doImportEos(TextSource textSource, EditorFrame editor, String url, int id, String baseHtml, boolean writeDelays)
			throws IOException {

		Pattern pattern = Pattern.compile("data-author-id=\"(\\d{1,7})\"");
		Matcher matcher = pattern.matcher(baseHtml);
		if (!matcher.find()) {
			throw new IllegalArgumentException("No author ?");
		}

		String imageFolder = "milovana/";
		String mp3Folder = "milovana/";
		imageFolder += id + "/";
		mp3Folder += id + "/";
		Set<String> images = new HashSet<String>();
		Set<String> mp3s = new HashSet<String>();

		String firstColor = null;
		Pattern patternFirstColor = Pattern.compile("color:\\s?#([A-Fa-f0-9]+)");
		
		String scriptHtml = editor.getUrlContent("https://milovana.com/webteases/geteosscript.php?id=" + id);
		if (scriptHtml.length() < 100)
			throw new IllegalArgumentException("No script ?");
		JsonSlurper jsonSlurper = new JsonSlurper();
		Map<String, Object> json = (Map<String, Object>) jsonSlurper.parseText(scriptHtml);
		Map<String, Object> pages = (Map<String, Object>) json.get("pages");

		String content = "";
		String startBlock = "";
		int n = 0;
		for (String pageName : pages.keySet()) {
			boolean goneSomewhere = false;
			boolean delayWasUsed = false;
			boolean hasDelay = false;
			boolean delayIsSecret = false;
			String delayDurationStr = null;
			String blockAfterDelayStr = null;
			String image = null, mp3 = null;
			
			content += "case \"" + pageName + "\":{->";

			for (Map<String, Object> item : (List<Map<String, Object>>) pages.get(pageName)) {
				boolean anyPause = false;
				String text = null;
				if (item.containsKey("say")) {
					Map<String, String> subItem = (Map<String, String>) item.get("say");
					if (subItem.containsKey("label")) {
						text = subItem.get("label");
						// groovy
						text = text.replace("$", "\\$");
						// java
						text = text.replace("\"", "\\\"");
						// strange bug, show "cant&apost" for example for can't
						text = text.replace("&apos;", "'");						
						Matcher mFirstColor = patternFirstColor.matcher(text);
						if (mFirstColor.find())
							firstColor = mFirstColor.group(1);
				
						for (int s = 5; s < 40; s++)
							text = text.replace("SIZE=\\\"" + s + "\\\"", "SIZE=\\\"" + ((int) (2 + s / 4)) + "\\\"");
						content += "\n\tshow(\"" + text + "\")";
					}
				}
				
				if (item.containsKey("image")) {
					try {						
						String imageStr = ((Map<String, String>) item.get("image")).get("locator"); 
						String imageUrl = null;
						if(imageStr.startsWith("gallery:")) {
							String [] parts = imageStr.substring(8).split("/");
							Map<String,Object> gallery = ((Map<String,Map<String,Object>>)json.get("galleries")).get(parts[0]);
							if(gallery!=null && gallery.containsKey("images")) {
								for(Map<String,String> imageJson : (List<Map<String,String>>)gallery.get("images")) {
									if(imageJson.containsKey("id") && String.valueOf(imageJson.get("id")).equals(parts[1])) {
										imageUrl = "https://media.milovana.com/timg/tb_xl/"+
												imageJson.get("hash")+".jpg?origin=https://eosscript.com";
									}
								}
							}
						} else if(imageStr.startsWith("file:")) {
							String key = imageStr.substring(5);
							Map<String,Map<String,String>> files = (Map<String,Map<String,String>>)json.get("files");
							Map<String,String> file = files.get(key);
							if(file!=null)
								imageUrl = "https://media.milovana.com/timg/tb_xl/"+
										file.get("hash")+".jpg?origin=https://eosscript.com";							
						}
						if(imageUrl!=null)
							image = getDownloadedFile(MainWindow.IMAGE_FOLDER + imageFolder, n, images, imageUrl, editor);
						if(image != null) {
							if (image.contains("*"))
								content += "\n\tsetImage(\"" + imageFolder + image + "\".replace(\"*\",\"\"+getRandom("
										+ STAR_RANDOM_NB_RESOURCES + "))" + ")";
							else
								content += "\n\tsetImage(\"" + imageFolder + image + "\")";
						}
				
					} catch (IOException e) {
					}
				}
				
				if (item.containsKey("audio.play")) {
					try {						
						String mp3Str = ((Map<String, String>) item.get("audio.play")).get("locator"); 
						String mp3Url = null;
						if(mp3Str.startsWith("file:")) {
							String key = mp3Str.substring(5);
							Map<String,Map<String,String>> files = (Map<String,Map<String,String>>)json.get("files");
							Map<String,String> file = files.get(key);
							mp3Url = "https://media.milovana.com/timg/"+
								file.get("hash")+".mp3";							
						}
						mp3 = getDownloadedFile(MainWindow.SOUND_FOLDER + mp3Folder, n, mp3s, mp3Url, editor);
						if(mp3 != null) {
							if (mp3.contains("*"))
								content += "\n\tplayBackgroundSound(\"" + mp3Folder + mp3 + "\".replace(\"*\",\"\"+getRandom("
										+ STAR_RANDOM_NB_RESOURCES + "))" + ")";
							else
								content += "\n\tplayBackgroundSound(\"" + mp3Folder + mp3 + "\")";
						}

					} catch (IOException e) {
					}
				}
				
				if (item.containsKey("goto")) {
					Map<String, String> subItem = (Map<String, String>) item.get("goto");
					if (subItem.containsKey("target")) {
						String nextPage = subItem.get("target");
						content += "\n\tblock=\"" + nextPage + "\"";
						goneSomewhere = true;
					}
				}

				if (item.containsKey("choice")) {
					Map<String, Object> subChoiceItem = (Map<String, Object>) item.get("choice");
					if (subChoiceItem.containsKey("options")) {

						content += "\n\tdef values" + n + "=[]";
						content += "\n\tdef pages" + n + "=[]";
						for (Map<String, Object> subSubItem : (List<Map<String, Object>>) subChoiceItem.get("options")) {
							content += "\n\tvalues" + n + ".add(\"" + ((String) subSubItem.get("label")) + "\")";
							content += "\n\tpages" + n + ".add(\""
									+ ((List<Map<String, Map<String,String>>>) 
											subSubItem.get("commands")).get(0).get("goto").get("target")
									+ "\")";
						}
					}
					content += "\n\tblock = pages" + n + "[getSelectedValue(null, values" + n + ")]";
					goneSomewhere = true;
					anyPause = true;
				}
				
				if (item.containsKey("timer")) {
					hasDelay = true;
					Map<String, Object> subItem = (Map<String, Object>) item.get("timer");
					if (subItem.containsKey("commands")) {
						Map<String, Object> command = ((List<Map<String, Object>>)subItem.get("commands")).get(0);
						if(command.containsKey("goto"))
							blockAfterDelayStr = "block=\""+((Map<String, String>)command.get("goto")).get("target")+"\"";
						if(command.containsKey("choice")) {
							Map<String, Object> subChoiceItem = (Map<String, Object>) command.get("choice");
							if (subChoiceItem.containsKey("options")) {
								content += "\n\tdef values" + n + "=[]";
								content += "\n\tdef pages" + n + "=[]";
								for (Map<String, Object> subSubItem : (List<Map<String, Object>>) subChoiceItem.get("options")) {
									content += "\n\tvalues" + n + ".add(\"" + ((String) subSubItem.get("label")) + "\")";
									content += "\n\tpages" + n + ".add(\""
											+ ((List<Map<String, Map<String,String>>>) 
													subSubItem.get("commands")).get(0).get("goto").get("target")
											+ "\")";
								}
							}
							content += "\n\tblock = pages" + n + "[getSelectedValue(null, values" + n + ")]";
							goneSomewhere = true;
						}
					}
					delayIsSecret = (subItem.containsKey("style") && ((String)subItem.get("style")).equals("secret") );
					String delaySrc = ((String)subItem.get("duration")).trim();
					if(!delaySrc.contains("-")) { //ex : 10s
						delayDurationStr = String.valueOf(getSecondsFromString(delaySrc));
					} else { //ex : "10s-1m"
						String[] limitsSrc = delaySrc.split("-");
						float[] limits = new float[2];
						for(int i=0;i<2;i++) {
							limits[i] = getSecondsFromString(limitsSrc[i]);
						}
						delayDurationStr = limits[0]+"+getRandom("+(limits[1]-limits[0])+")";
					}
					anyPause = true;
					hasDelay = true; //how to do better with choice + timer ?
				}

				if( item.containsKey("nyx.page")) {

					Map<String, Object> nyxItem = (Map<String, Object>) item.get("nyx.page");
					if (nyxItem.containsKey("text")) {
						text = (String)(nyxItem.get("text"));
						// groovy
						text = text.replace("$", "\\$");
						// java
						text = text.replace("\"", "\\\"");
						// strange bug, show "cant&apost" for example for can't
						text = text.replace("&apos;", "'");
						Matcher mFirstColor = patternFirstColor.matcher(text);
						if (mFirstColor.find())
							firstColor = mFirstColor.group(1);
				
						for (int s = 5; s < 40; s++)
							text = text.replace("SIZE=\\\"" + s + "\\\"", "SIZE=\\\"" + ((int) (2 + s / 4)) + "\\\"");
						content += "\n\tshow(\"" + text + "\")";
					}
					
					if (nyxItem.containsKey("media")) {
						try {						
							String imageStr = ((Map<String, String>) nyxItem.get("media")).get("nyx.image"); 
							String imageUrl = null;
							if(imageStr.startsWith("gallery:")) {
								String [] parts = imageStr.substring(8).split("/");
								Map<String,Object> gallery = ((Map<String,Map<String,Object>>)json.get("galleries")).get(parts[0]);
								for(Map<String,String> imageJson : (List<Map<String,String>>)gallery.get("images")) {
									if(imageJson.containsKey("id") && String.valueOf(imageJson.get("id")).equals(parts[1])) {
										imageUrl = "https://media.milovana.com/timg/tb_xl/"+
												imageJson.get("hash")+".jpg?origin=https://eosscript.com";
									}
								}							
							} else if(imageStr.startsWith("file:")) {
								String key = imageStr.substring(5);
								Map<String,Map<String,String>> files = (Map<String,Map<String,String>>)json.get("files");
								Map<String,String> file = files.get(key);
								if(file!=null)
									imageUrl = "https://media.milovana.com/timg/tb_xl/"+
											file.get("hash")+".jpg?origin=https://eosscript.com";							
							}
							if(imageUrl!=null)
								image = getDownloadedFile(MainWindow.IMAGE_FOLDER + imageFolder, n, images, imageUrl, editor);
							if(image != null) {
								if (image.contains("*"))
									content += "\n\tsetImage(\"" + imageFolder + image + "\".replace(\"*\",\"\"+getRandom("
											+ STAR_RANDOM_NB_RESOURCES + "))" + ")";
								else
									content += "\n\tsetImage(\"" + imageFolder + image + "\")";
							}
					
						} catch (IOException e) {
						}
					}
					
					if (nyxItem.containsKey("hidden")) {
						try {						
							String mp3Str = ((Map<String, Map<String,String>>) nyxItem.get("hidden")).get("audio.play").get("locator"); 
							String mp3Url = null;
							if(mp3Str.startsWith("file:")) {
								String key = mp3Str.substring(5);
								Map<String,Map<String,String>> files = (Map<String,Map<String,String>>)json.get("files");
								Map<String,String> file = files.get(key);
								mp3Url = "https://media.milovana.com/timg/"+
									file.get("hash")+".mp3";							
							}
							mp3 = getDownloadedFile(MainWindow.SOUND_FOLDER + mp3Folder, n, mp3s, mp3Url, editor);
							if(mp3 != null) {
								if (mp3.contains("*"))
									content += "\n\tplayBackgroundSound(\"" + mp3Folder + mp3 + "\".replace(\"*\",\"\"+getRandom("
											+ STAR_RANDOM_NB_RESOURCES + "))" + ")";
								else
									content += "\n\tplayBackgroundSound(\"" + mp3Folder + mp3 + "\")";
							}

						} catch (IOException e) {
						}
					}
					
					if (nyxItem.containsKey("goto")) {
						Map<String, String> subItem = (Map<String, String>) nyxItem.get("goto");
						if (subItem.containsKey("target")) {
							String nextPage = subItem.get("target");
							content += "\n\tblock=\"" + nextPage + "\"";
							goneSomewhere = true;
						}
					}

					if (nyxItem.containsKey("action") ) {
						Map<String, Object> action = (Map<String, Object>) nyxItem.get("action");

						if (action.containsKey("nyx.buttons")) {
							List<Map<String,Object>> subChoiceItems = (List<Map<String,Object>>) action.get("nyx.buttons");
							if(subChoiceItems.size()==1) {
								content += "\n\tshowButton(\""+subChoiceItems.get(0).get("label")+"\")";
								content += "\n\tblock = \""+((List<Map<String, Map<String,String>>>) 
										subChoiceItems.get(0).get("commands")).get(0).get("goto").get("target") +"\"";								
							} else {
								content += "\n\tdef values" + n + "=[]";
								content += "\n\tdef pages" + n + "=[]";
								for (Map<String, Object> subSubItem : subChoiceItems) {
									content += "\n\tvalues" + n + ".add(\"" + ((String) subSubItem.get("label")) + "\")";
									content += "\n\tpages" + n + ".add(\""
											+ ((List<Map<String, Map<String,String>>>) 
													subSubItem.get("commands")).get(0).get("goto").get("target")
											+ "\")";
								}
								content += "\n\tblock = pages" + n + "[getSelectedValue(null, values" + n + ")]";
							}
							goneSomewhere = true;
							anyPause = true;
						}
						
						if (action.containsKey("nyx.timer")) {
							Map<String, Object> subItem = (Map<String, Object>) action.get("nyx.timer"); 
							if (subItem.containsKey("commands")) {
								Map<String, Object> command = ((List<Map<String, Object>>)subItem.get("commands")).get(0);
								if(command.containsKey("goto")) {
									blockAfterDelayStr = "block=\""+((Map<String, String>)command.get("goto")).get("target")+"\"";
									goneSomewhere = true;
								}
							}
							delayIsSecret = (subItem.containsKey("style") && ((String)subItem.get("style")).equals("secret") );
							String delaySrc = ((String)subItem.get("duration")).trim();
							if(!delaySrc.contains("-")) { //ex : 10s
								delayDurationStr = String.valueOf(getSecondsFromString(delaySrc));
							} else { //ex : "10s-1m"
								String[] limitsSrc = delaySrc.split("-");
								float[] limits = new float[2];
								for(int i=0;i<2;i++) {
									limits[i] = getSecondsFromString(limitsSrc[i]);
								}
								delayDurationStr = limits[0]+"+getRandom("+(limits[1]-limits[0])+")";
							}
							anyPause = true;
							hasDelay = true; 
						}
						
						if (action.containsKey("nyx.vert")) {
							hasDelay = true;
							List<Map<String, Object>> elements = ((Map<String,List<Map<String, Object>>>) action.get("nyx.vert")).get("elements");
							Map<String, Object> elemMap = new HashMap<String, Object>();
							for(Map<String, Object> e:elements) {
								String oneKey = e.keySet().iterator().next(); 
								elemMap.put(oneKey, e.get(oneKey));
							}
									
							if (elemMap.containsKey("nyx.buttons")) {
								List<Map<String,Object>> subChoiceItems = (List<Map<String,Object>>) elemMap.get("nyx.buttons");
								if(subChoiceItems.size()==1) {
									content += "\n\tshowButton(\""+subChoiceItems.get(0).get("label")+"\")";
									content += "\n\tblock = \""+((List<Map<String, Map<String,String>>>) 
											subChoiceItems.get(0).get("commands")).get(0).get("goto").get("target") +"\"";								
								} else {
									content += "\n\tdef values" + n + "=[]";
									content += "\n\tdef pages" + n + "=[]";
									for (Map<String, Object> subSubItem : subChoiceItems) {
										content += "\n\tvalues" + n + ".add(\"" + ((String) subSubItem.get("label")) + "\")";
										content += "\n\tpages" + n + ".add(\""
												+ ((List<Map<String, Map<String,String>>>) 
														subSubItem.get("commands")).get(0).get("goto").get("target")
												+ "\")";
									}
									content += "\n\tblock = pages" + n + "[getSelectedValue(null, values" + n + ")]";
								}
								goneSomewhere = true;
								anyPause = true;
							}
								
							if (elemMap.containsKey("nyx.timer")) {
								Map<String, Object> subItem = (Map<String, Object>) elemMap.get("nyx.timer"); 
								delayIsSecret = (subItem.containsKey("style") && ((String)subItem.get("style")).equals("secret") );
								String delaySrc = ((String)subItem.get("duration")).trim();
								if(!delaySrc.contains("-")) { //ex : 10s
									delayDurationStr = String.valueOf(getSecondsFromString(delaySrc));
								} else { //ex : "10s-1m"
									String[] limitsSrc = delaySrc.split("-");
									float[] limits = new float[2];
									for(int i=0;i<2;i++) {
										limits[i] = getSecondsFromString(limitsSrc[i]);
									}
									delayDurationStr = limits[0]+"+getRandom("+(limits[1]-limits[0])+")";
								}
								anyPause = true;
							}
						}
					}
				}
				
				
				if (hasDelay && !delayWasUsed && blockAfterDelayStr != null) {
					if (delayIsSecret)
						content += "\n\twait(" + delayDurationStr + ")";
					else
						content += "\n\twaitWithGauge(" + delayDurationStr + ")";
					content += "\n\t" + blockAfterDelayStr;
					goneSomewhere = true;
					anyPause = true;
				}

				/*
				 * 
				 * if(item.containsKey("commands")){ for(Map<String,Object>
				 * subItem :(List<Map<String,Object>>) item.get("commands")) {
				 * if(subItem.containsKey("goto")) { Map<String,String>
				 * subSubItem = (Map<String,String>) subItem.get("goto");
				 * if(subSubItem.containsKey("target")) { String nextPage =
				 * subSubItem.get("target"); content += "\n\tblock=\"" +
				 * nextPage + "\""; goneSomewhere = true; } } } }
				 */
				
				if(text!= null && !anyPause) {
					if (writeDelays && mp3 == null)
						content += "\n\twait(" + (int) (MILOVANA_BASE_DELAY + text.length() / MILOVANA_DELAY_LETTERS_1S) + ")";
					if (!writeDelays)
						content += "\n\tshowButton(\"Continue\")";
				}			
			}
			if (!goneSomewhere)
				content += "\n\tendReached = true";
			if (mp3 != null && !mp3.equals(""))
				content += "\n\tplayBackgroundSound(null);";			
			content += "\n\t}(); break\n\n";
			if (startBlock.equals(""))
				startBlock = pageName;
			n++;
		}

		editor.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		// watch blue component of first text color to say dark or light
		if (firstColor != null && Integer.parseInt(firstColor, 16) % 256 > 127)
			editor.addInfo("OK, you should set a dark background in setInfo() ");
		else
			editor.addInfo("OK");
		
		return "\n\n// Milovana - " + url + "\n// " + textSource.getString("milovana.rights") + "\n"
				+ "def endReached = false\n" + "def block = \"" + startBlock + "\"\n" + "def newBlock = block\n"
				+ "while(!endReached && newBlock!=null) {\n" + "block = null\n" + "switch(newBlock) {\n" + content
				+ "default:\n" + "\tshow(\"Unfinished ? (\"+newBlock+\")\")\n"
				+ "\tendReached = true\n} // end switch \n" + "newBlock = block\n" + "} // end while \n"
				+ "wait(1)\nshowButton(\"Close\")\nshow(\"\")\n";

	}
	
	/**
	 * 
	 * @param str 30s, 20m, 5m30s...
	 * @return int value
	 */
	private float getSecondsFromString(String str) {
		float v = 0;
		if(str.contains("h")) {
			v += 3600 * Integer.parseInt(str.substring(0,str.indexOf("h")));
			if(!str.endsWith("h"))
				str = str.substring(str.indexOf("h")+1);
		}	
		if(str.contains("m") && (str.indexOf("m")==str.length()-1 || str.charAt(str.indexOf("m")+1)!='s')) {
			v += 60 * Integer.parseInt(str.substring(0,str.indexOf("m")));
			if(!str.endsWith("m"))
				str = str.substring(str.indexOf("m")+1);
		}
		if(str.contains("s") && str.charAt(str.indexOf("s")-1)!='m') {
			v += Float.parseFloat(str.substring(0,str.indexOf("s")));
			if(!str.endsWith("s"))
				str = str.substring(str.indexOf("s")+1);
		}
		if(str.contains("ms")) {
			v += Float.parseFloat(str.substring(0,str.indexOf("ms")))/1000f;
			if(!str.endsWith("ms"))
				str = str.substring(str.indexOf("ms")+1);
		}
//		System.out.println(str+" => "+v);
		return v;
	}

	private String getDownloadedFile(String folder, int n, Set<String> files, String url, EditorFrame editor)
			throws FileNotFoundException, IOException, MalformedURLException {
		String filename = url;
		if (filename.contains("?"))
			filename = filename.substring(0, url.indexOf("?"));
		filename = filename.substring(filename.lastIndexOf("/") + 1);
		if (filename.contains("name="))
			filename = filename.substring(filename.indexOf("name=") + 5);
		if (filename.contains("*"))
			return getDownloadedFileMultiple(folder, files, url, filename);
		else
			return getDownloadedFileSingle(folder, n, files, url, filename, editor);
	}

	private String getDownloadedFileSingle(String folder, int n, Set<String> files, String url, String filename,
			EditorFrame editor) throws FileNotFoundException, IOException, MalformedURLException {
		filename = ((n + 1) * 10) + "-" + filename;
		File f = new File(folder);
		f.mkdirs();
		f = new File(f, filename);
		if(!NOCACHE && f.exists())
			return filename; 
		OutputStream os = new FileOutputStream(f);
		HttpURLConnection connection = editor.getHttpURLConnection(url);
		InputStream is = connection.getInputStream();
		int c = 0;
		while ((c = is.read()) > -1)
			os.write(c);
		is.close();
		os.close();
		String foundDuplicate = null;
		for (String i : files)
			if (new File(folder + i).length() == f.length())
				foundDuplicate = i;
		if (foundDuplicate == null) {
			files.add(filename);
		} else {
			f.delete();
			filename = foundDuplicate;
		}
		return filename;
	}

	private String getDownloadedFileMultiple(String folder, Set<String> files, String url, String filename)
			throws FileNotFoundException, IOException, MalformedURLException {
		filename = filename.replace("*", "_").replace(".", "*.");
		while (filename.lastIndexOf("*") != filename.indexOf("*"))
			filename = filename.replaceFirst("\\*", "");
		for (int i = 0; i < STAR_RANDOM_NB_RESOURCES; i++) {
			File f = new File(folder);
			f.mkdirs();
			f = new File(f, filename.replace("*", String.valueOf(i)));
			// do not reload each time, only sometime
			if (!f.exists() || (Math.random() < .1 && NOCACHE)) {
				OutputStream os = new FileOutputStream(f);
				HttpURLConnection connection = editor.getHttpURLConnection(url);
				InputStream is = connection.getInputStream();
				int c = 0;
				while ((c = is.read()) > -1)
					os.write(c);
				is.close();
				os.close();
			}
		}
		return filename;
	}

}
