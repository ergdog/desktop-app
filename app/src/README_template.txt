SexScripts - modified software
Sources: https://gitlab.com/ergdog/desktop-app
Build 0.0.${MINOR_VERSION_PLACEHOLDER}

Forked from:
----------------------
SexScripts - main software
See https://ss.deviatenow.com
1.30 / API 9 - by Doti
----------------------
GNU GPLv3 ; USE AT YOUR OWN RISK

To launch the application:
- Open your terminal
- Change directory to the location of this readme file
- Execute the start script corresponding to your operating system:

macOS and *nix:
./sexscripts

Windows:
sexscripts.bat
