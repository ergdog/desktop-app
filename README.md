# desktop-app

This project is forked from https://gitlab.com/dotidn/desktop-app with the following changes: 
- Uses alternative webcam driver `webcam-capture-driver-native` to try to fix webcam issues in macOS
- Uses Gradle to build and package the application
- Targets Java 11+

## Requirements
You will need a modern Java installation such as [Eclipse Temurin](https://adoptium.net/) to run the application.

The application has been tested with JRE versions 11 and 17. However, running with JRE 21 is currently broken due to deprecations.

## ZIP Package
A pre-built ZIP format package can be found in the [Package Registry](https://gitlab.com/ergdog/desktop-app/-/packages)

Alternatively, for instructions on building, running and packaging the application locally, please refer to the section below.

## Development Instructions

### Building the Application

To build the application, use the following command:

```
./gradlew build
```

### Running the Application

To run the application for local development, use the following command:

```
./gradlew run
```

### Packaging the Application

To package the application into a distributable ZIP format, execute the following command:

```
./gradlew assemble
```

### Creating an Uber JAR

To create a single main application JAR file containing all JAR dependencies, but without data dependencies such as images and scripts, execute the following command:

```
./gradlew uberJar
```

## Gradle details

For detailed instructions on building Java & JVM projects with gradle, please refer to the [Gradle documentation](https://docs.gradle.org/8.6/userguide/building_java_projects.html).

### Plugins

This project utilizes the following plugins:

- `java`: Used for building Java projects.
- `application`: Applied to add support for building a CLI application in Java.

### Repositories

This project uses Maven Central for resolving dependencies.

### Dependencies

This project has dependencies on the following libraries:

- Groovy Core: `org.codehaus.groovy:groovy`
- Groovy JSON: `org.codehaus.groovy:groovy-json`
- Groovy Datetime: `org.codehaus.groovy:groovy-datetime`
- Groovy Dateutil: `org.codehaus.groovy:groovy-dateutil`
- JLayer: `javazoom:jlayer`
- INI4J: `org.ini4j:ini4j`
- Webcam Capture: `com.github.sarxos:webcam-capture`
- Webcam Capture Driver Native: `io.github.eduramiba:webcam-capture-driver-native`

### Runtime Configuration

- Directory used for data when running locally using the 'run' task: `run`
- Data file: `data.zip` contains application images, scripts, sounds and videos.

### Java Toolchain

A specific Java toolchain is applied to ease working on different environments. The language version used is Java 11.

### Custom Tasks

- `uberJar`: Creates a single JAR file with all dependencies included. This task collects all dependencies from the `runtimeClasspath` configuration and packages them into one JAR file.
- `createData`: Copies data from the `data.zip` file into the build directory.
- `createRuntimeDir`: Copies data from `data.zip` into the project directory for runtime.
- `cleanRuntimeDir`: Deletes the runtime data directory.
- `run`: Configured to depend on `createRuntimeDir`, sets working directory to `run`, and passes `-debug` argument.
- `clean`: Configured to depend on `cleanRuntimeDir`.
- `writeVersionToReadme`: Writes the CI pipeline IID (if available) to the README file. This task filters the `README_template.txt`, replacing `${MINOR_VERSION_PLACEHOLDER}` with the CI pipeline IID obtained from the environment variable `CI_PIPELINE_IID` or '0' if not available.

### Application Configuration

- Main class: `ss.desktop.Main`
- Start scripts are written to the base directory.
- Application default JVM arguments: `-Xdock:name=SexScripts` sets the application name in macOS.

### Testing

The project uses JUnit Platform for unit tests.

For further details and configurations, refer to the `build.gradle` file.

Enjoy developing with Gradle!